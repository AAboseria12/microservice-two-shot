# Wardrobify

Team:

* Abdallah Abouseria - hats
* Anastasia Dwarica-Pham - shoes

## Design

## Shoes microservice

Set up a poller component to trigger regular requests to the wardrobe API.
Configure the poller to fetch shoe collection updates and changes.
Update the application state with the retrieved shoe data.

Create a Shoe model class to represent shoe items.
Define attributes such as style name, color, picture URL, and location.

Develop RESTful APIs to manage the location information of shoes in the wardrobe. These APIs will allow CRUD (Create, Read, Update, Delete) operations.

For the frontend, creating a React component named ShoesList. This component was responsible for displaying a list of all the shoes present in the API, along with their associated details. Features can delete shoes directly from the list.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Model for fabric, its style name, its color, a URL for a picture, and the location in the wardrobe where it exists.The location already has its Restful APIs finished. Start with the poller, then the model, then the RESTful APIs, lastly use react to build the frontend. Also document my steps as I move ahead for future reference
