# Generated by Django 4.0.3 on 2023-08-30 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locationvo',
            old_name='name',
            new_name='closet_name',
        ),
        migrations.AlterField(
            model_name='hat',
            name='picture',
            field=models.URLField(blank=True, null=True),
        ),
    ]
