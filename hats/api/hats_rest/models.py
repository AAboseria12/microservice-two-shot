from django.db import models

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Hat(models.Model):
    fabric = models.CharField(max_length=150)
    style =  models.CharField(max_length=150)
    color =  models.CharField(max_length=150)
    picture = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name= "hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style
