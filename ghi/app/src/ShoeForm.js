import React, { useEffect, useState } from "react";

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [binNumber, setBin] = useState('');
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [pictureurl, setPictureUrl] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.model_name = name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureurl;
        data.bin = binNumber;



        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchOptions);
        if (response.ok) {
            const newShoe = await response.json();

            setName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');
        }
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }




    useEffect(() => {
        fetchData();
    }, []);




    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add New Shoe</h1>
                    <form onSubmit={handleSubmit} id="add-new-shoe-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Shoe name" required type="text" id="name" name="model_name" className="form-control" />
                            <label htmlFor="name">Shoe Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureurl} onChange={handlePictureUrlChange} placeholder="pictureurl" required type="url" id="pictureurl" name="picture_url" className="form-control" />
                            <label htmlFor="pictureurl">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select value={binNumber} onChange={handleBinChange} placeholder="bin" required id="bin" name="bin_number" className="form-select">
                                <option value="">Choose bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>{bin.id}</option>
                                    )
                                })}

                            </select>
                        </div>
                        <button className="btn btn-primary">Add shoe</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm;
