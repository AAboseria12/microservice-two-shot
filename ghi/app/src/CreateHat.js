import React, { useEffect, useState } from 'react';

function CreateHat() {
    const [locations, setLocations] = useState([]);
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [locationNumber, setLocationNumber] = useState('');

    const handleChangeFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleChangeStyle = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangePicture = (event) => {
      const value = event.target.value;
      setPicture(value)
    }

    const handleChangeLocationNumber = (event) => {
        const value = event.target.value;
        setLocationNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.fabric = fabric;
            data.style = style;
            data.color = color;
            data.picture = picture;
            data.location = locationNumber;


        const hatsURL = 'http://localhost:8090/api/hats/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const hatResponse = await fetch(hatsURL, fetchOptions);
        if (hatResponse.ok) {
            const newHat = await hatResponse.json();
            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocationNumber('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <h1>Add a Hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChangeFabric} value={fabric} placeholder="Fabric" required name="fabric" type="text" id="fabric" className='form-control' />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangeStyle} value={style} placeholder="Style" required name="style" type="text" id="style" className='form-control' />
                    <label htmlFor="style">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangeColor} value={color} placeholder="Color" required name="color" type="text" id="color" className='form-control' />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangePicture} value={picture} placeholder="Picture" required name="picture" type="text" id="picture" className='form-control' />
                    <label htmlFor="picture">Picture url</label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleChangeLocationNumber} value={locationNumber} placeholder="Location Number" required name="location_number" type="text" id="location_number" className='form-control'>
                        <option value="">Select a location</option>
                        {locations.map((location) => {
                            return (
                                <option key={location.href} value={location.id}>
                                    {location.closet_name}
                                </option>
                                    )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create Hat</button>
            </form>

        </>
    );
}

export default CreateHat;
